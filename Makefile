bin/vida: obj/main.o obj/funciones.o obj/util.o
	gcc -Wall -fsanitize=address obj/main.o obj/funciones.o obj/util.o -o bin/vida -lm

obj/main.o: src/main.c
	gcc -Wall -c -I include/ src/main.c -o obj/main.o

obj/funciones.o: src/funciones.c
	gcc -Wall -c -I include/ src/funciones.c -o obj/funciones.o

obj/util.o: src/util.c
	gcc -Wall -c -I include/ src/util.c -o obj/util.o

.PHONY: clean
clean:
	rm bin/* obj/*

