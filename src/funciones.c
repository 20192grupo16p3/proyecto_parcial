#include <stdio.h>
#include <stdlib.h>
#include "funciones.h"
#include "util.h"

void ** vivir_morir(char **grilla, int filas, int columnas,int generaciones){
	int vecinos;
	int celulas_vivas;
	int celulas_muertas;
	int cv=0;
	int cm=0;
	int promedio_vivas;
	int promedio_muertas;
//primero la nueva matriz y tiene que ser llena de ceros.
	char **grilla_nueva=(char**)calloc(filas,sizeof(char*));
	for (int c=0; c<filas; c++){
        	*(grilla_nueva+c)=(char *)calloc(columnas,sizeof(char));
	}
	int g=1;
	int gc=1;
	//for(int g=0;g<generaciones;g++){
	while(g<=generaciones || generaciones <=0){
	celulas_vivas=0;
	celulas_muertas=0;
	for(int i=0;i<filas;i++){
		for(int j=0;j<columnas;j++){
			vecinos=0;
			if(i==0 && j==0){
			//(aqui va la esquina superior izquierda)
				for(int k=0;k<2;k++){
					for(int l=0;l<2;l++){
						if(*(*(grilla+k)+l)==1){vecinos++;}
						}
					}
				if(*(*(grilla+i)+j)==1){vecinos--;} //(esto es para que reste si esta viva ya que igual la contara para poder usar los for.)
			}

			else if(i==0 && j==(columnas-1)){
			//(aqui va la esquina superior derecha)
				for(int m=0;m<2;m++){
					for(int n=columnas-2;n<columnas;n++){
						if(*(*(grilla+m)+n)==1){vecinos++;}
					}
				}
				if(*(*(grilla+i)+j)==1){vecinos--;}
			}

			else if(i==(filas-1) && j==0){
			//(aqui va la esquina inferior izquierda)
				for(int o=filas-2;o<filas;o++){
					for(int p=0;p<2;p++){
						if(*(*(grilla+o)+p)==1){vecinos++;}
					}
				}
				if(*(*(grilla+i)+j)==1){vecinos--;}
			}

			else if(i==(filas-1) && j==(columnas-1)){
			//(aqui va la esquina inferior derecha)
				for(int q=filas-2;q<filas;q++){
					for(int r=columnas-2;r<columnas;r++){
						if(*(*(grilla+q)+r)==1){vecinos++;}
					}
				}
				if(*(*(grilla+i)+j)==1){vecinos--;}
			}

			else if(i==0 && (j>0 && j<(columnas-1))){
			//(fila superior sin esquinas)
				for(int s=0;s<2;s++){
					for(int t=-1;t<2;t++){
						if(*(*(grilla+s)+j+t)==1){vecinos++;}
					}
				}
				if(*(*(grilla+i)+j)==1){vecinos--;}
			}

			else if(i==(filas-1) && (j>0 && j<(columnas-1))){
			//(fila inferior sin esquinas)
				for(int u=filas-2;u<filas;u++){
					for(int v=-1;v<2;v++){
						if(*(*(grilla+u)+j+v)==1){vecinos++;}
					}
				}
				if(*(*(grilla+i)+j)==1){vecinos--;}
			}

			else if((i>0 && i<(filas-1)) && j==0){
			//(columna lateral izquierda sin esquinas)
				for(int w=-1;w<2;w++){
					for(int x=0;x<2;x++){
						if(*(*(grilla+i+w)+x)==1){vecinos++;}
					}
				}
				if(*(*(grilla+i)+j)==1){vecinos--;}
			}

			else if((i>0 && i<(filas-1)) && j==(columnas-1)){
			//(columna lateral derecha sin esquinas)
				for(int y=-1;y<2;y++){
					for(int z=columnas-2;z<columnas;z++){
						if(*(*(grilla+i+y)+z)==1){vecinos++;}
					}
				}
				if(*(*(grilla+i)+j)==1){vecinos--;}
			}

			else{
			//(cualquiera que tenga los 8 lados es decir todas las que no estan en los bordes)
				for(int a=-1;a<2;a++){
					for(int b=-1;b<2;b++){
						if(*(*(grilla+i+a)+j+b)==1){vecinos++;}
					}
				}
				if(*(*(grilla+i)+j)==1){vecinos--;}
			}
		
		//	if(*(*(grilla+i)+j)==1){vecinos--;} //(esto es para que reste si esta viva ya que igual la contara para poder usar los for.)

//aqui ya tienes la cantidad de vecinos del punto que se esta revisando
//entonces aqui ya comienzas a validar las funciones de las reglas segun si estaba viva o muerta, te dejo mas o menos como seria
			//printf("\n fila: %d  columna: %d  vecinos: %d", i,j,vecinos);
			if(*(*(grilla+i)+j)==1){
//aqui va las reglas de las vivas segun sus vecinos y la asignas a la nueva matriz
				if(vecinos<=1){//muere por soledad
					*(*(grilla_nueva+i)+j)=0;
					celulas_muertas++;
				}else{
					if(vecinos>=4){
						*(*(grilla_nueva+i)+j)=0;//muere por sobrepoblacion
						celulas_muertas++;
					}else{*(*(grilla_nueva+i)+j)=1;
						celulas_vivas++;}//celula vive
				}
			}
			else{
			//aqui van las reglas si estaba muerta segun sus vecinos y la asignas a la nueva matriz
				if(vecinos==3){
					*(*(grilla_nueva+i)+j)=1;
					celulas_vivas++;
				}else{*(*(grilla_nueva+i)+j)=0;}
			}

		}//aqui se cierra el for de columnas
	}//aqui se cierra el for de filas
	
	dibujar_grilla(grilla_nueva,filas,columnas);
	//free(grilla_nueva);
	for(int u=0;u<filas;u++){
		for(int v=0;v<columnas;v++){
			*(*(grilla+u)+v)=*(*(grilla_nueva+u)+v);}
		}

	cv=cv+celulas_vivas;
	cm=cm+celulas_muertas;
	promedio_vivas=cv/(gc);
	promedio_muertas=cm/(gc);
	if(generaciones>0){g++;}
	else{printf("\n Infinitas Generaciones.....");}
	printf("\n\nGeneracion: %d\n Celulas vivas en esta generacion: %d\n Celulas que murieron en esta generacion: %d\n",(gc),celulas_vivas,celulas_muertas);
	printf(" Celulas vivas desde la generacion 0: %d\n Celulas muertas desde la generacion 0: %d\n",cv,cm);
	printf(" Promedio de celulas vivas por Generacion: %d\n Promedio de celulas muertas por generacion: %d\n",promedio_vivas,promedio_muertas);
	usleep(75000);
	//grilla=grilla_nueva;
	gc++;
	}//aqui se ciera el while de generaciones
	for(int i=0; i<filas; i++){
		free(*(grilla_nueva+i));
		}
	free(grilla_nueva);
	return 0;
}




