#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "vida.h"
#include "util.h"
#include "funciones.h"

//comienzo main
int main(int argc, char **argv){
	long c=0;//columnas
	long f=0;//filas
	long g=0;//generaciones
	long s=0;//microsegundos
	int  nCel=0;//numeroDeCelulas
	char op;
	while((op=getopt(argc,argv,"f:c:g:s:i:"))!=-1){
		switch(op){
			case 'f':
				f=(atoi(optarg));
				break;
			case 'c':
				c=(atoi(optarg));
				break;
			case 'g':
				g=(atoi(optarg));
				break;
			case 's':
				s=(atoi(optarg));
				break;
			case 'i':
				nCel=(atoi(optarg));
				break;
		}
	}
	//validaciones
	if(f<=0||c<=0||s<=0||nCel<=0){
		printf("\nIngreso un valor incorrecto, porfavor intentelo nuevamente\n\n");
		printf("Valores a ingresar: \n -f (filas >0) \n -c (columnas>0) \n -s (tiempo entre generaciones en milisegundos>0) \n -i (cantidad de celulas vivas inicialmente>0) \n -g (generaciones infinitas<=0; generaciones finitas>0)\n \n");
		exit(-1);//se cierra el programa
	}
	//fin validaciones
	//inicia el juego si todos los valores son correctos.
	juego_de_vida juego;
        juego.filas=f;//10
        juego.columnas=c;//20
        juego.tiempo_sleep=s;//40000
        juego.generaciones=g;//20
	//char**tablero=malloc(juego.filas*sizeof(char*));
        //for (int i=0; i<juego.filas; i++){
                //*(tablero+i)=malloc(juego.columnas*sizeof(char));
        	//}
	//for(int i=0; i<juego.generaciones;i++){
		char**tablero=malloc(juego.filas*sizeof(char*));
        	for (int i=0; i<juego.filas; i++){
                	*(tablero+i)=malloc(juego.columnas*sizeof(char));
                	}
		llenar_matriz_azar(tablero,juego.filas,juego.columnas,nCel);
		dibujar_grilla(tablero,juego.filas,juego.columnas);
		usleep(1000000);
		vivir_morir(tablero, juego.filas, juego.columnas,juego.generaciones);
	//for(int i=0; i<juego.generaciones;i++){
		//if(i>0){vivir_morir(, juego.filas, juego.columnas);}
		for(int i=0; i<juego.filas; i++){
			free(*(tablero+i));
			}
		free(tablero);
		//dibujar_grilla(grilla_nueva,juego.filas,juego.columnas);
		//free(grilla_nueva);
		//usleep(1000000);
		//}
		//}
}
